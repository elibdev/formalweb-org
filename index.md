---
title: Own your platform.
---

## Empower your voice.

Build on a platform that doesn't lock you in.

Formal Web is a user-friendly __100% open source__ website builder
and content management system (CMS) built on a 
foundation of modular components and open specifications.

Sign up for your free website today, no credit card required.
You can upgrade to premium hosting on a custom domain for __$5/month__.

## More info coming soon.

Email `eli (at) frml.co` for more info.